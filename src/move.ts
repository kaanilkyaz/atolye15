// Please update this type as same as with the data shape.
type File = { id: string; name: string };
type List = Array<{ id: string; name: string; files: Array<File> }>;
let sourceFile: File;

function findSourceFileAndRemoveFromCurrentFolder(list: List, source: string) {
  list.forEach((folder) => {
    if (folder.id === source) throw new Error('You cannot move a folder');
    folder.files.forEach((file, index) => {
      if (file.id === source) {
        sourceFile = file;
        folder.files.splice(index, 1);
      }
    });
  });

  return sourceFile;
}
export default function move(list: List, source: string, destination: string): List {
  sourceFile = findSourceFileAndRemoveFromCurrentFolder(list, source);
  const destinationFolderIndex = list.findIndex((folder) => folder.id === destination);

  if (destinationFolderIndex < 0) throw new Error('You cannot specify a file as the destination');
  list[destinationFolderIndex].files.push(sourceFile);

  return list;
}
